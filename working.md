# Version Control in Scientific Software

### Tim Storer and Tom Wallis, University of Glasgow 

### SoftMech Workshop, 12th December 2017

GitLab project: [https://gitlab.com/twsswt/softmech_git_tutorial](https://gitlab.com/twsswt/softmech_git_tutorial)

---

# Why is Version Control Important in Science?

Science, like the world, is inherently messy, but software is now an essential tool in *all* experimental work and
adds both considerable capability *and* complexity.  As a consequence, good software version control is essential for
repeatable, reproducable scientific results.

>...trying to convert .glo files to .grim files so that we can compare with previous output. However the progam suite
>headed by globulk.f90 is not playing nicely - problems with it expecting a defunct file system (all path widths were
>80ch, have been globally changed to 160ch) and also no guidance on which reference files to choose. It also doesn't
>seem to like files being in any directory other than the current one!!...''

HARRY\_READ\_ME.txt, 'Climategate' Archive, c2009.

Version control is useful for tracking how documents change over time. It records the changes made in blocks called
"commits", and will allow you to move backwards or forwards in time by reversing the changes in those "commits". That
means that if your code becomes broken, or you've made a mistake somewhere, it's easily reverted. So, version control
systems literally help to control different versions of your documents.

Version control is also often hosted in the cloud --- this makes it a kind of primitive backup system, which is useful in
a bind! (Though *not* what you should rely on for backups.) Lastly, when version control is stored in the cloud, it makes
collaborating with other people much easier, because version control systems can help to "merge" the changes ("commits")
made by different contributors without breaking the project.

Paper: Tim Storer. 2017.
[Bridging the Chasm: A Survey of Software Engineering Practice in Scientific Programming.](https://doi.org/10.1145/3084225)
ACM Computing. Surveys. 50, 4, Article 47 (August 2017).

# What to use Version Control for?

Software engineers talk about *configuration items*: broadly speaking, anything you *directly* edit should be subject to
version control, for example:

 * Source code
 * Configuration scripts
 * Dependency description files for tools like Make, Cran and Pip. 
 * Paper source files (LaTeX, Word, Markdown...)
 * Small data sets

These can be managed using source/software configuration/control management or version control systems.  In practice,
these terms are all synonyms.

These can all be managed using version control systems, which are sometimes called source/software configuration/control
management systems. There are lots of options, all with their own pros and cons --- we will focus on Git.

---

# Some Preliminaries


Although Git is emerging as the defacto standard for version control is very powerful and I don't actually like Git so
much; it:

  * is very complex
  * solves a large number of version control problems that many users will *never* experience
  * has a user interface written with an expectation that you have read (and memorised) the manual
  * has many *overloaded* commands (that do more than one thing)

I will make mistakes and become baffled during this tutorial (so will Tom).  Please bear with us!

## Installing Git

  * [http://git-scm.com/download/win](Windows)
  * [http://git-scm.com/download/mac](Mac)
  * On Ubuntu/Linux, type `sudo apt-get install git-all` into a shell.

See the [manual](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) for more information.

## Sign up for GitLab at [http://gitlab.com](http://gitlab.com)

Just about everything I will talk about in [GitLab](http://gitlab.com) has an equivalent in GitHub, but GitLab has some
neat extra features (including free private repositories).

---

# In a Single Repository

 Diagram

---

# Git Commits or Changesets

A changeset (or more properly, a *commit*) is a particular snapshot of a project that a user decides to 'commit' to the
repository. It represents all of the changes since the *last* commit --- so we get a history over time of how the project
progresses.

In Git, every changeset has a guaranteed unique label, for example:

`98d3e53db295ec58e8dfbf19fe22725e7849bc85`

This value is generated using a hash function called SHA-1.  The function accepts as input all the content in the
working directory when a changeset is created, along with metadata, such as the username of the committer, the date of
the commit and the log message of the commit; and generates a fixed length output that is guraranteed to be unique.

`SHA-1(username, date, message) -> hash`

A nice explanation of the architecture of a Git repository can be found
[here](https://blog.thoughtram.io/git/2014/11/18/the-anatomy-of-a-git-commit.html).

---

# Configuring Git

Git options can either be set for a repository, or globally.

For example:

```
$ git config --global user.name twsswt
$ git config --global user.email timothy.storer@glasgow.ac.uk
```

# Local Operations

  * *Initialise* a repository in any directory using `init`
  * *Stage* a change to a file using `add <filename>`
  * *Commit* a change using `commit`
  * Get useful information with `status` and `log`

```
$ cd ~/Documents/
$ mkdir git_tutorial_notes
$ cd git_tutorial
$ git init
$ touch README.md
```

Having a `README.md` is a *good practice*.  Git and GitLab will encourage other good practices, as we will see.

Now make some changes to the new README.md file, perhaps by copying any notes you've made so far into it.

```
$ git status
$ git add README.md
$ git status
$ git commit -m "Added README.md"
```

---

# Undoing Changes

  * *Remove* a change from staging using `git reset <filename>`
  * *Revert* a file after a commit using `checkout` and `commit`


Make some more changes to the file - perhaps add a title `# Notes on Git`.

```
$ git add README.md
$ git reset README.md
$ git status
```

Now decide that you really do want to commit the changes.

```
$ git add README.md
$ git commit -m "Updated README.md with Title"
```

Oops, no, you've changed your mind, so let's revert them by one commit.

```
$ git log
$ git checkout master~1 README.md
$ git status
$ git add README.md
$ git commit -m "Reverted Title Formatting."
```

---

# Peer to Peer Model

Diagram

---

# Commands for Linking to a Remote Repository

Communication via http, https or ssh+git.

 * Link to a remote repository using `git remote add origin <url>`
 * Clone an existing repository using `git clone <url>`
 * Transfer changes to the remote repository with `git push`
 * Transfer changes to the local repository (and merge) with `git pull`
 * Get useful information with `remote show <branch>`

Two ways of linking with an existing remote repository:

```
$ cd ~/Documents/
$ git clone https://gitlab.com/twsswt/softmech_git_tutorial.git
```

or:

```
$ cd ~/Documents/
$ git init softmech_git_tutorial
$ cd softmech_git_tutorial
$ git remote add origin https://gitlab.com/twsswt/softmech_git_tutorial.git
$ git pull
```

Why two ways?  The first is useful when you want to work on an existing repository, perhaps provided by a third party.
The second is useful when you want to import your code into an (empty) remote repository for the first time.

---

# Getting Information About a Remote Repository

 * Get useful information with `remote show <branch>`

```
git remote show origin
```

Example:

```
* remote origin
  Fetch URL: https://gitlab.com/twsswt/softmech_git_tutorial
  Push  URL: https://gitlab.com/twsswt/softmech_git_tutorial
  HEAD branch: master
  Remote branch:
    master tracked
  Local branch configured for 'git pull':
    master merges with remote master
  Local ref configured for 'git push':
    master pushes to master (up to date)
```

---

# Good Practices:

## Frequent Small Changes, Pull and Push

 * Limits merge conflicts
 * Concise commit messages summaries

## Meaningful Commit Message

See https://chris.beams.io/posts/git-commit/ for some  excellent advice.

  * Use a text editor, rather than the `-m` option.

    ```$ git config --global core.editor nano```

  * The first line should be a title/subject of no more than 50 characters, followed by a blank line.
  * Wrap the body at 72 characters.
  * Explain the high level *what* and *why* not the how.

Example:

```
Added Draft Results Section

This is the first version of the results, based on the simulation runs
generated from
https://github.com/twsswt/simucake/releases/tag/r1.1.1. Graphs are
included for sweetness versus sugar content in recipe; and sweetness
versus peanut butter content.
```

---

# What *shouldn't* you use version control for?

Anything that you don't directly edit:

 * Artefacts derived from configuration items, e.g. program binaries, paper PDFs
 * Third party libraries
 * Data sets collected from experimental setups or monitoring
 * Development environment configuration files, e.g. `.eclipse`, `.autosave`

---

# Using `.gitignore`

A file that lists any files that should be ignored by Git (i.e. files not subject to version control).

  * Can be placed anywhere in the project directory structure.
  * In practice, most people place it in the root directory.
  * Can use `glob` patterns and `# comments`

Example:

```
# Python Files
*.pyc
```

There is a useful repository of .gitignore templates on GitHub's [gitignore](https://github.com/github/gitignore) project.

---

# Merge Conflicts

So far, we've only talked about one line of development.  Sometimes somebody will push changes to a repository that you
are working one.  Consider the following scenario:

1. Tim clones REPO on GitLab
2. Tom clones REPO on GitLab
3. Tim edits file README.md, changing line 3 from `Ana Ibrahim` to `Ana Ibrahim and Tim Storer`
4. Tim stages, commits and pushes the change.
5. Tom edits file README.md, changing line 3 from `Ana Ibrahim` to `Ana Ibrahim and Tom Wallis`
6. Tom stages and commits the file.  Now Tom tries a push...
7. Git tells Tom to pull changes that Ana pushed.  When he does this, he ends up with a message like this:

```
git pull
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
Automatic merge failed; fix conflicts and then commit the result.
```

and a file like this:

```
<<<<<<< HEAD
Ana Ibrahim, Tom Wallis
=======
Ana Ibrahim, Tim Storer
>>>>>>> a6969ec0d266034186a0a277f7b1f561825b5cd6
```

Tom will now need to edit this (either manually or with a diff tool) and then stage, commit and push the resulting change, e.g:

```
Ana Ibrahim, Tom Wallis and Tim Storer
```

---

# Branches

Branches are parallel development efforts.  They can be started from any existing commit.

 * Create a new branch using `git branch <name>`
 * Switch to a branch using `git checkout <name>`
 * Merge from a branch using `git merge name`
 * Notify the remote repository using `git push --setupstream <remote> <branch>`

```
git branch sec-overview
git checkout sec-overview
git push --set-upstream origin sec-overview
```

 To keep things simple, it is a good idea to link to a branch of the same name.

```
git checkout master
git merge sec-overview
git commit
git push
```

Good practice: commit to branches and merge to master.

---

# Good Practices: Versions versus Releases

 * *Versions* are the changesets in your Git repository
 * *Releases* are labelled changesets in your git repository that are intended for public consumption.

Releases are often published to dependency management libraries, such as PyPy for Python, or Mvn Maven.

It is good practice to only link to particular releases of a project and to reference these in experimental
documentation (i.e. papers).

## Tagging

Git supports *tagging* of a commit as a release on the command line:

```
git tag v0.1
```

but, it is better to create a release on GitLab, where you can add supplementary information, such as compiled artefacts.

## [Semantic Versioning](https://semver.org) Scheme

`<major>.<minor>.<incremental>`


  * Major should be bumped for *breaking changes*
  * Minor should be bumped for *non-breaking* feature additions
  * Incremental should be bumped for *bug-fixes*

---

# Online Repository Services

 * [GitLab] Many useful features, including free private repos, but often a bit slower than...
 * [GitHub] Lots of nice stats and graphs, but lacks free private repos
 * [BitBucket]

Tour of GitLab...

  * Tracking comments on projects, papers as issues.
  * Creating and moderating pull requests.
  * Creating a release
  * Enabling a GitLab runner.

---

# Good Practice: Organising Work into Separate Repositories

It is good practice to have separate repositories for each 'logically distinct' work package in your research.  This
reduces coupling between activities.

For example:

  * Your thesis (there is a [LaTeX template](https://github.com/sdstrowes/Glasgow-Thesis-Template)) for University of
    Glasgow theses on GitHub
  * Code that generates raw data
  * Code that processes or analyses data
  * Each paper that you write
  * Code that converts an analysis into a graph for a specific paper or your thesis.

If your code is used to generate a large data set, create a *release* for the code and *attach* the data set as an
archive to the release on GitLab.


---

# Good Practice: Enabling a GitLab Runner

Create a file in your root directory called `.gitlab-ci.yml` for generating compiled artefacts, e.g. HTML from markdown:

```
image: ntwrkguru/pandoc-gitlab-ci

SLIDY-HTML:
  script:
    - pandoc -f markdown -t slidy -s --self-contained -o working.html working.md
  artifacts:
    paths:
      - "*.html"
```
	  
---

# Interactive Staging

(Later versions of Git only).

Suppose you forget to stage a commit after making a change and now have two sets of changes in your working copy that it
would be better to commit separately.

```
git add -p README.md
```

---

# Git workflows and lifecycles

Useful to note are the different layers that your changes go through between simply changing a file locally, and making sure
those changes are kept up-to-date in a place like Gitlab. There are four:

1. Workspace. This is where your changes live when you just change a file or two --- your workspace is literally the current
   state of your files.
2. Index. This is where Git *keeps track* of your changes, if you add something to eventually be committed, but it doesn't take
   a snapshot of those changes.
3. Local repository. This is the collection of all of the changes that your documents have undergone --- when you commit, a
   snapshot of whatever's in your Index goes here.
   Note that this means that if you don't `add` something to your Index, it won't be committed! That means you can omit things
   you don't want to save in git.
4. Remote repository. Like your local repository, but somewhere else (i.e. the cloud). If you `push`, your local repository is 
   synced with the remote location (for us, this is Gitlab).
   
So, moving changes from:

* Workspace to Index -> `git add`
* Index to Local Repository -> `git commit`
* Workspace to Local Repository (combining `add` and `commit`) -> `git commit -a`
* Local Repository to Remote Repository -> `git push`
* Remote repository to Local repository -> `git fetch`
* Remote repository to Workspace -> `git pull`
  * This is much more common than `fetch`!
  * Usually when you retrieve changes from the cloud, you want to see those changes reflected in your filesystem.
* Local repository to Workspace -> `git checkout HEAD`
* Index to Workspace -> `git checkout`
* If you want to *see* the changes, try `git diff` to see the differences between different stages of the workflows.

A useful little diagram to help remember:

![Git workflows](workflow_diagram.png)

--- 

# Other Useful Resources

 * [Git Documentation](https://git-scm.com/book)
 * [GitLab Documentation](https://docs.gitlab.com)
 * [Software Carpentry Workshop](https://software-carpentry.org)

